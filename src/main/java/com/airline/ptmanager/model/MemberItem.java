package com.airline.ptmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class MemberItem {
    private Long id;

    private String memberName;

    private String memberPhone;
    private Float Weight;
    private Float height;
    private LocalDateTime dateFirst;
    private LocalDateTime dateLast;
    private Double bmi;
    private String bmiResultName;

}
