package com.airline.ptmanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberInfoUpdateRequest {

    private String memberName;
    private String memberPhone;
}
