package com.airline.ptmanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberWeightUpdateRequest {
    private Float weight;
}
