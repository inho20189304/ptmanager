package com.airline.ptmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberRegistration {
    @NotNull
    @Length(min = 2, max = 20)
    private String memberName;
    @NotNull
    @Length(min = 11, max = 20)
    private String memberPhone;
    @NotNull
    private Float height;
    @NotNull
    private Float weight;

}
