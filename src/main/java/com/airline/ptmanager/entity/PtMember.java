package com.airline.ptmanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class PtMember {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String memberName;
    @Column(nullable = false, length = 20)
    private String memberPhone;
    @Column(nullable = false)
    private Float height;

    @Column(nullable = false)
    private Float Weight;
    @Column(nullable = false)
    private LocalDateTime dateFirst;
    private LocalDateTime dateLast;






}
