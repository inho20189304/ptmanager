package com.airline.ptmanager.controller;

import com.airline.ptmanager.model.MemberInfoUpdateRequest;
import com.airline.ptmanager.model.MemberItem;
import com.airline.ptmanager.model.MemberRegistration;
import com.airline.ptmanager.model.MemberWeightUpdateRequest;
import com.airline.ptmanager.service.PtMemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/ptmember")
public class PtMemberController {
    private final PtMemberService ptMemberService;

    @PostMapping("/data")
    public String setPtMember(@RequestBody @Valid MemberRegistration registration) {
      ptMemberService.setPtMember(registration);

      return "ok";
    }
    @GetMapping("/all")
    public List<MemberItem> getPtMembers() {
        List<MemberItem> result = ptMemberService.getPtMembers();

        return result;
    }
    @PutMapping("/info/id/{id}")
    public String putMemberInfo(@PathVariable long id, @RequestBody @Valid MemberInfoUpdateRequest request) {
        ptMemberService.putMemberInfo(id, request);

        return "Success!";
    }
    @PutMapping("/weight/id/{id}")
    public String putMemberWeight(@PathVariable long id, @RequestBody @Valid MemberWeightUpdateRequest request) {
        ptMemberService.putMemberWeight(id, request);

        return "Success!";
    }
    @PutMapping("/visit/id/{id}")
    public String putMemberVisit(@PathVariable long id) {
        ptMemberService.putMemberVisit(id);

        return "Success!";
    }
}
