package com.airline.ptmanager.service;

import com.airline.ptmanager.entity.PtMember;
import com.airline.ptmanager.model.MemberInfoUpdateRequest;
import com.airline.ptmanager.model.MemberItem;
import com.airline.ptmanager.model.MemberRegistration;
import com.airline.ptmanager.model.MemberWeightUpdateRequest;
import com.airline.ptmanager.repository.PtMemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PtMemberService {
    private final PtMemberRepository ptMemberRepository;

    public void setPtMember(MemberRegistration registration) {
        PtMember addData = new PtMember();
        addData.setMemberName(registration.getMemberName());
        addData.setMemberPhone(registration.getMemberPhone());
        addData.setHeight(registration.getHeight());
        addData.setWeight(registration.getWeight());
        addData.setDateFirst(LocalDateTime.now());

        ptMemberRepository.save(addData);
    }
    public List<MemberItem> getPtMembers() {
        List<PtMember> originList = ptMemberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();

        for (PtMember item : originList) {
            MemberItem addItem = new MemberItem();
            addItem.setId(item.getId());
            addItem.setMemberName(item.getMemberName());
            addItem.setMemberPhone(item.getMemberPhone());
            addItem.setHeight(item.getHeight());
            addItem.setWeight(item.getWeight());
            addItem.setDateFirst(item.getDateFirst());
            addItem.setDateLast(item.getDateLast());

            float heightDouble = (item.getHeight() / 100) *(item.getHeight() / 100);
            double bmi = item.getWeight() / heightDouble;

            addItem.setBmi(bmi);

            String bmiResultText = "";
            if (bmi <= 18.5) {
                bmiResultText = "저체중";
            } else if (bmi <= 22.9) {
                bmiResultText = "정상";
            } else if (bmi <= 24.9) {
                bmiResultText = "과체중";
            } else {
                bmiResultText = "비만";
            }
            addItem.setBmiResultName(bmiResultText);
            result.add(addItem);
        }
        return result;
    }

    public void putMemberInfo(long id, MemberInfoUpdateRequest request) {
        PtMember originData = ptMemberRepository.findById(id).orElseThrow();
        originData.setMemberName(request.getMemberName());
        originData.setMemberPhone(request.getMemberPhone());

        ptMemberRepository.save(originData);
    }
    public void putMemberWeight(long id, MemberWeightUpdateRequest request) {
        PtMember originData = ptMemberRepository.findById(id).orElseThrow();
        originData.setWeight(request.getWeight());

        ptMemberRepository.save(originData);
    }
    public void putMemberVisit(long id) {
        PtMember originData = ptMemberRepository.findById(id).orElseThrow();
        originData.setDateLast(LocalDateTime.now());

        ptMemberRepository.save(originData);
    }

}
