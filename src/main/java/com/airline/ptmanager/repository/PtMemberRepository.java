package com.airline.ptmanager.repository;

import com.airline.ptmanager.entity.PtMember;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PtMemberRepository extends JpaRepository<PtMember, Long> {
}
